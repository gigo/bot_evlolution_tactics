# BotEvolutionTactics

Build & Deployment Stack

  * Tool version manager: [asdf](https://github.com/asdf-vm/asdf)
  * Pakcage release: [Distillery](https://github.com/bitwalker/distillery)
  * Deployment: [edeliver](https://github.com/edeliver/edeliver)


Build Server Setup

  * {Instruction to create a linux user for building}
  * Manually setup `asdf` tools manager (ref: https://github.com/asdf-vm/asdf)
  * Manually install required packages: `make automake autoconf libreadline-dev libncurses-dev libssl-dev libyaml-dev libxslt-dev libffi-dev build-essential g++ libtool unixodbc-dev python unzip`
  * Manually add `asdf` plugins: `asdf plugin-add`, the plugins are `elixir`, `nodejs`, `erlang`

App Server Setup

  * {Instruction to create a linux user for web app}


Note that the environment of Build Server and App Server should be identical.


## Deployment Steps (Production Server)

  * Push all your commits
  * `mix edeliver build release` to build app in build server
  * `mix edeliver deploy release to production` to deploy package to app server
  * `mix edeliver start production` to start app in app server for the first time
  * `mix edeliver restart production` to restart app once you deploy a new version
  * `mix edeliver migrate production` to migrate database
