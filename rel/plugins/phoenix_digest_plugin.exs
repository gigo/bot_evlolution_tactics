defmodule BotEvolutionTactics.PhoenixDigestPlugin do
  use Mix.Releases.Plugin

  def before_assembly(%Release{} = _release) do
    info "before assembly!"

    case System.cmd("npm", ["run", "deploy"], cd: "assets") do
      {output, 0} ->
        info output
        Mix.Task.run("phx.digest")
        nil
      {output, error_code} ->
        {:error, output, error_code}
    end
  end

end
