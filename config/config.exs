# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :bot_evolution_tactics,
  ecto_repos: [BotEvolutionTactics.Repo]

# Configures the endpoint
config :bot_evolution_tactics, BotEvolutionTacticsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "4eKMj6ZUi2N/s8dP/aVRuT9H0dvItT1SY/JVM/E3f/yK/yMUpR1ITyAw0dCIHWkQ",
  render_errors: [view: BotEvolutionTacticsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: BotEvolutionTactics.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id],
  backends: [:console, Rollbax.Logger, {LoggerFileBackend, :log_file}]

config :logger, :log_file,
  format: "$message\n",
  path: "/var/log/bot_evolution_tactics/access.log",
  level: :info

# We configure the Rollbax.Logger backend.
config :logger, Rollbax.Logger,
  level: :error

config :logster, :filter_parameters, ["password", "secret", "token"]

# Default Rollbax setup, this is expected to be overwriteen by rollbax.secret.exs
config :rollbax,
  access_token: "none",
  environment: "none"

# The secret key field is set in 'guardian.secret.exs'
config :guardian, Guardian,
  allowed_algos: ["HS512"], # optional
  verify_module: Guardian.JWT,  # optional
  issuer: "BotEvolutionTactics",
  ttl: { 30, :days },
  allowed_drift: 2000,
  verify_issuer: true, # optional
  serializer: BotEvolutionTactics.GuardianSerializer,
  hooks: BotEvolutionTactics.GuardianHooks

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
import_config "line.secret.exs"
import_config "guardian.secret.exs"
