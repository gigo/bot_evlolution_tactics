use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :bot_evolution_tactics, BotEvolutionTacticsWeb.Endpoint,
  http: [port: 4001],
  server: false

# Disable rollbax
config :rollbax, enabled: :log

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :bot_evolution_tactics, BotEvolutionTactics.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "bot_evolution_tactics_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
