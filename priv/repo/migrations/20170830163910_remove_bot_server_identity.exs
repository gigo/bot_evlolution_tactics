defmodule BotEvolutionTactics.Repo.Migrations.RemoveBotServerIdentity do
  use Ecto.Migration

  def change do
    alter table(:bots) do
      remove :server_identity
    end
  end
end
