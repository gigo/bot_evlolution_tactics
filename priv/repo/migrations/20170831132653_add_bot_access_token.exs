defmodule BotEvolutionTactics.Repo.Migrations.AddBotAccessToken do
  use Ecto.Migration

  def change do
    alter table(:bots) do
      add :access_token, :string, comment: "The access token of the bot"
      add :account_id,   :string, comment: "It's the id of the bot on the platform"
    end
  end
end
