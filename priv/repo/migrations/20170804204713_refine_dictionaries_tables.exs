defmodule BotEvolutionTactics.Repo.Migrations.RefineDictionariesTables do
  use Ecto.Migration

  def change do
    alter table(:bots) do
      remove :knowledge_id
    end

    drop_if_exists table(:dictionaries)
    drop_if_exists table(:word_responses)
    drop_if_exists table(:knowledge)

    create table(:dictionaries) do
      add :name,           :string,  null: false,  comment: "name of the dictionary, for the sake of readable"
      timestamps()
    end
    create unique_index(:dictionaries, :name)

    # many-to-many relations between dictionaries and bots
    create table(:bots_dictionaries) do
      add :bot_id,        references(:bots, on_delete: :delete_all)
      add :dictionary_id, references(:dictionaries, on_delete: :delete_all)

      add :apply_order,   :integer, default: 0, comment: "the order of the dictionaries to be applied"

      timestamps()
    end
    create index(:bots_dictionaries, :bot_id)
    create index(:bots_dictionaries, :dictionary_id)
    create unique_index(:bots_dictionaries, [:bot_id, :dictionary_id])

    create table(:vocabularies) do
      add :word,           :string,  null: false,       comment: "Define the word which trigger reponse once we receive"
      add :emotion_impact, :integer, default: 0,        comment: "Once receive the word, how much it affects to the bot emotion"

      add :dictionary_id,  references(:dictionaries, on_delete: :delete_all)
      timestamps()
    end

    create table(:reactions) do
      add :type,          :string,   null: false, comment: "response type: word / weather / daily_news ... etc"
      add :response_word, :string,                comment: "which word to response"

      add :vocabulary_id, references(:vocabularies, on_delete: :delete_all)
      timestamps()
    end
  end
end
