defmodule BotEvolutionTactics.Repo.Migrations.AddStaffs do
  use Ecto.Migration

  def change do
    create table(:staffs, comment: "Staffs who manage the admin pages") do
      add :name,          :string, null: false, comment: "Name of the staff"
      add :username,      :string, null: false, comment: "Username for login"
      add :password,      :string,              comment: "A field existed for nothing"
      add :password_hash, :string, null: false, comment: "Encrypted password"
      add :last_login_at, :utc_datetime,   comment: "Last login time"

      timestamps()
    end

    create unique_index(:staffs, :username)
  end
end
