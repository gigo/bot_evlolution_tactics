defmodule BotEvolutionTactics.Repo.Migrations.AddBots do
  use Ecto.Migration

  def change do
    create table(:bots, comment: "Bot properties") do
      add :name,         :string, null: false, comment: "A name of the bot for distinguish"
      add :platform,     :string, null: false, comment: "The platform of the bot, like fb/line ... etc"
      add :secret_key,   :string,              comment: "The secret key of the bot"

      add :server_identity, :string,           comment: "ID of the server, so we can know the request really comes from it"
      add :knowledge_id, :integer,             comment: "Which knowledge the bot would use"

      timestamps()
    end

    create table(:knowledge, comment: "Contains dictionaries") do
      add :name,          :string, null: false, comment: "name of the knowledge, for the sake of readable"
      add :dictionary_id, :integer,             comment: "associate with dictionary"

      timestamps()
    end

    create table(:word_responses, comment: "Action to take when we receive a word") do
      add :type,          :string,   null: false, comment: "response type: word / weather / daily_news"
      add :response_word, :string,                comment: "which word for response"
      add :dictionary_id, :integer,  null: false, comment: "belongs to which dictionary"

      timestamps()
    end

    create table(:dictionaries, comment: "Define relations between words and response action") do
      add :word,           :string,  null: false,       comment: "Define the word which trigger reponse once we receive"
      add :emotion_impact, :integer, default: 0,        comment: "Once receive the word, how much it affects to the bot emotion"
      add :word_responses, references(:word_responses), comment: "Has many response"

      timestamps()
    end

  end
end
