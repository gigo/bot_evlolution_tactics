defmodule BotEvolutionTactics.Factories.VocabularyFactory do
  use ExMachina.Ecto, repo: BotEvolutionTactics.Repo

  def vocabulary_factory do
    %BotEvolutionTactics.Dictionaries.Vocabulary {
      word: "foo",
      emotion_impact: 0
    }
  end

end
