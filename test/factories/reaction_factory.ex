defmodule BotEvolutionTactics.Factories.ReactionFactory do
  use ExMachina.Ecto, repo: BotEvolutionTactics.Repo

  alias BotEvolutionTactics.Dictionaries.Reaction

  def word_reaction_factory do
    %Reaction {
      type: "word",
      response_word: "hello"
    }
  end

  def weather_reaction_factory do
    %Reaction {
      type: "weather"
    }
  end

  def daily_news_reaction_factory do
     %Reaction {
      type: "daily_news"
    }
  end
end
