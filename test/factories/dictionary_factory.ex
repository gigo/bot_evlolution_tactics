defmodule BotEvolutionTactics.Factories.DictionaryFactory do
  use ExMachina.Ecto, repo: BotEvolutionTactics.Repo

  def dictionary_factory do
    %BotEvolutionTactics.Dictionaries.Dictionary {
      name: sequence(:name, &"dict_name#{&1}")
    }
  end
end
