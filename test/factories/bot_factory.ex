defmodule BotEvolutionTactics.Factories.BotFactory do
  use ExMachina.Ecto, repo: BotEvolutionTactics.Repo

  def bot_factory do
    %BotEvolutionTactics.BotAccounts.Bot {
      name: "good bot",
      platform: "line",
      secret_key: "aaabbbccc"
    }
  end
end
