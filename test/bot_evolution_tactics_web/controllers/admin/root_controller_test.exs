defmodule BotEvolutionTacticsWeb.Admin.RootControllerTest do
  use BotEvolutionTacticsWeb.ConnCase
  use BotEvolutionTactics.StaffSessionCase

  setup %{conn: conn} do
    create_staff_and_login(conn, name: "michael")
  end

  describe "index" do
    test "display root page", %{conn: conn} do
      conn = get conn, admin_root_path(conn, :index)
      assert html_response(conn, 200) =~ "michael"
    end
  end
end
