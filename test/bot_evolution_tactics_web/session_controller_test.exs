defmodule BotEvolutionTacticsWeb.SessionControllerTest do
  use BotEvolutionTacticsWeb.ConnCase

  alias BotEvolutionTactics.Repo
  alias BotEvolutionTactics.Staffs.Staff


  test "GET /sessions/new", %{conn: conn} do
    conn = get conn, "/sessions/new"
    assert html_response(conn, 200) =~ "Login page"
  end

  test "perform login", %{conn: conn} do
    # create a user first
    Staff.changeset(%Staff{}, %{username: "foo", password: "bar", name: "nothing"})
    |> Repo.insert

    conn = post conn, "/sessions", %{"username" => "foo", password: "bar"}

    # it's redirect
    assert html_response(conn, 302)

    {_, location} = conn.resp_headers
                    |> Enum.find(fn h -> elem(h, 0) == "location" end)

    # the redirect link should be admin root
    assert location == "/admin"

    # the staff last_login_at should be updated
    staff = Staff
            |> where([username: "foo"])
            |> Repo.one

    refute staff.last_login_at == nil
  end
end
