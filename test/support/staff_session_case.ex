defmodule BotEvolutionTactics.StaffSessionCase do
  @moduledoc """
  This module defines functions related to staff sessions.

  The login mechanism borrowed from:
  https://github.com/elixir-plug/plug/blob/2181abb0e30b774f7640add3e5a5401567a87aca/test/plug/session/cookie_test.exs#L31-L35
  """

  use Plug.Test
  use ExUnit.CaseTemplate
  alias BotEvolutionTactics.Repo
  alias BotEvolutionTactics.Staffs.Staff


  using do
    quote do
      alias BotEvolutionTactics.Repo
      alias BotEvolutionTactics.Staffs.Staff

      import Ecto
      import Ecto.Changeset
      import Ecto.Query
      import BotEvolutionTactics.StaffSessionCase
    end
  end

  @default_opts [
    store: :cookie,
    key: "foobar",
    encryption_salt: "encrypted cookie salt",
    signing_salt: "signing salt"
  ]

  @secret String.duplicate("abcdef0123456789", 8)
  @signing_opts Plug.Session.init(Keyword.put(@default_opts, :encrypt, false))

  @default_staff_attrs %{username: "foo", password: "bar", name: "baz"}

  def create_staff_and_login(conn, attrs \\ %{}) do
    attrs = Enum.into(attrs, @default_staff_attrs)

    {:ok, staff} =
      %Staff{}
      |> Staff.changeset(attrs)
      |> Repo.insert

    conn = conn
           |> sign_conn
           |> Guardian.Plug.sign_in(staff)

    {:ok, conn: conn}
  end

  defp sign_conn(conn, secret \\ @secret) do
    put_in(conn.secret_key_base, secret)
    |> Plug.Session.call(@signing_opts)
    |> fetch_session
  end
end
