defmodule BotEvolutionTactics.StaffTest do
  use BotEvolutionTactics.DataCase

  alias BotEvolutionTactics.Staffs.Staff
  alias BotEvolutionTactics.Repo

  @valid_attrs %{name: "foo", password: "bar", username: "boo"}

  test "changeset with valid attributes" do
    changeset = Staff.changeset(%Staff{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset without password" do
    changeset = Staff.changeset(%Staff{}, Map.delete(@valid_attrs, :password))
    refute changeset.valid?
  end

  test "changeset without username" do
    changeset = Staff.changeset(%Staff{}, Map.delete(@valid_attrs, :username))
    refute changeset.valid?
  end

  test "password should hash" do
    changeset = Staff.changeset(%Staff{}, @valid_attrs)

    password_hash = changeset.changes.password_hash
    refute password_hash == @valid_attrs.password
    refute String.length(password_hash) == 0
  end

  test "find and confirm password success" do
    changeset = Staff.changeset(%Staff{}, @valid_attrs)
    Repo.insert(changeset)

    {status, _ } = Staff.find_and_confirm_password(%{"username" => "boo", "password" => "bar"})
    assert status == :ok
  end

  test "cannot find specific user" do
    changeset = Staff.changeset(%Staff{}, @valid_attrs)
    Repo.insert(changeset)

    {status, _ } = Staff.find_and_confirm_password(%{"username" => "koo", "password" => "bar"})
    assert status == :error
  end

  test "find specific user with wrong password" do
    changeset = Staff.changeset(%Staff{}, @valid_attrs)
    Repo.insert(changeset)

    {status, _ } = Staff.find_and_confirm_password(%{"username" => "boo", "password" => "wtf"})
    assert status == :error
  end
end
