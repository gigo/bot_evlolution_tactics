defmodule BotEvolutionTactics.StaffsTest do
  use BotEvolutionTactics.DataCase

  alias BotEvolutionTactics.Staffs

  describe "staffs" do
    alias BotEvolutionTactics.Staffs.Staff

    @valid_attrs %{last_login_at: "2010-04-17 14:00:00.000000Z", name: "some name", password: "some password", username: "some username"}
    @update_attrs %{last_login_at: "2011-05-18 15:01:01.000000Z", name: "some updated name", password: "some updated password", username: "some updated username"}
    @invalid_attrs %{last_login_at: nil, name: nil, password: nil, username: nil}

    def staff_fixture(attrs \\ %{}) do
      {:ok, staff} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Staffs.create_staff()

      staff
    end

    test "list_staffs/0 returns all staffs" do
      staff = staff_fixture()
      assert Staffs.list_staffs() == [staff]
    end

    test "get_staff!/1 returns the staff with given id" do
      staff = staff_fixture()
      assert Staffs.get_staff!(staff.id) == staff
    end

    test "create_staff/1 with valid data creates a staff" do
      assert {:ok, %Staff{} = staff} = Staffs.create_staff(@valid_attrs)
      assert staff.last_login_at == DateTime.from_naive!(~N[2010-04-17 14:00:00.000000Z], "Etc/UTC")
      assert staff.name == "some name"
      assert staff.username == "some username"

      refute staff.password_hash == "" || staff.password_hash == nil || staff.password_hash == @valid_attrs.password
    end

    test "create_staff/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Staffs.create_staff(@invalid_attrs)
    end

    test "update_staff/2 with valid data updates the staff" do
      staff = staff_fixture()
      assert {:ok, staff} = Staffs.update_staff(staff, @update_attrs)
      assert %Staff{} = staff
      assert staff.last_login_at == DateTime.from_naive!(~N[2011-05-18 15:01:01.000000Z], "Etc/UTC")
      assert staff.name == "some updated name"
      assert staff.username == "some updated username"

      refute staff.password_hash == "" || staff.password_hash == nil || staff.password_hash == @valid_attrs.password
    end

    test "update_staff/2 with invalid data returns error changeset" do
      staff = staff_fixture()
      assert {:error, %Ecto.Changeset{}} = Staffs.update_staff(staff, @invalid_attrs)
      assert staff == Staffs.get_staff!(staff.id)
    end

    test "delete_staff/1 deletes the staff" do
      staff = staff_fixture()
      assert {:ok, %Staff{}} = Staffs.delete_staff(staff)
      assert_raise Ecto.NoResultsError, fn -> Staffs.get_staff!(staff.id) end
    end

    test "change_staff/1 returns a staff changeset" do
      staff = staff_fixture()
      assert %Ecto.Changeset{} = Staffs.change_staff(staff)
    end
  end
end
