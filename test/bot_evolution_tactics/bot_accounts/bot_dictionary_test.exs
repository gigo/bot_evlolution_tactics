defmodule BotEvolutionTactics.BotDictionaryTest do
  use BotEvolutionTactics.DataCase

  alias BotEvolutionTactics.Factories.BotFactory
  alias BotEvolutionTactics.Factories.DictionaryFactory

  alias BotEvolutionTactics.Repo
  alias BotEvolutionTactics.BotAccounts.Bot
  alias BotEvolutionTactics.Dictionaries.Dictionary

  import Ecto.Query

  describe "bot / dictionary association" do

    setup do
      bot = BotFactory.insert(:bot)
      {:ok, original_bot: bot}
    end

    setup %{original_bot: bot} do
      dictionary = DictionaryFactory.insert(:dictionary)

      Repo.insert! %BotEvolutionTactics.Dictionaries.BotDictionary {
        bot: bot,
        dictionary: dictionary
      }

      {:ok, original_dictionary: dictionary}
    end

    test "the connetion between bot/dictionary exists",
    %{original_bot: original_bot, original_dictionary: original_dictionary} do
      bot = Repo.one(from b in Bot, where: b.id == ^original_bot.id, preload: [:dictionaries])

      dictionary = bot.dictionaries |> Enum.at(0)
      assert dictionary == original_dictionary
    end

    test "the dictionary can reference back to bot",
    %{original_bot: original_bot, original_dictionary: original_dictionary} do
      dictionary = Repo.one(from d in Dictionary, where: d.id == ^original_dictionary.id, preload: [:bots])

      bot = dictionary.bots |> Enum.at(0)
      assert bot == original_bot
    end

  end

  describe "one bot has many dictionaries" do

    setup do
      bot = BotFactory.insert(:bot)
      dictionaries = DictionaryFactory.insert_list(2, :dictionary)

      Enum.each dictionaries, fn dict ->
        Repo.insert! %BotEvolutionTactics.Dictionaries.BotDictionary {
          bot: bot,
          dictionary: dict
        }
      end

      {:ok, original_bot: bot, original_dictionaries: dictionaries}
    end

    test "the bot can reference all dictionaries",
    %{original_bot: original_bot, original_dictionaries: original_dictionaries} do
      bot = Repo.one(from b in Bot, where: b.id == ^original_bot.id, preload: [:dictionaries])

      dictionaries = bot.dictionaries |> Enum.sort(&(&1.id <= &2.id))
      assert dictionaries == original_dictionaries |> Enum.sort(&(&1.id <= &2.id))
    end

  end

  describe "one dictionary belongs to many bots" do

    setup do
      dictionary = DictionaryFactory.insert(:dictionary)
      bots = BotFactory.insert_list(2, :bot)

      Enum.each bots, fn bot ->
        Repo.insert! %BotEvolutionTactics.Dictionaries.BotDictionary {
          bot: bot,
          dictionary: dictionary
        }
      end

      {:ok, original_bots: bots, original_dictionary: dictionary}
    end

    test "a dictionary reference to many bots",
    %{original_bots: original_bots, original_dictionary: original_dictionary} do
      dictionary = Repo.one(from d in Dictionary, where: d.id == ^original_dictionary.id, preload: [:bots])

      bots = dictionary.bots |> Enum.sort(&(&1.id <= &2.id))
      assert bots == original_bots |> Enum.sort(&(&1.id <= &2.id))
    end

  end

end
