defmodule BotEvolutionTactics.BotAccountsTest do
  use BotEvolutionTactics.DataCase

  alias BotEvolutionTactics.BotAccounts

  describe "bots" do
    alias BotEvolutionTactics.BotAccounts.Bot

    @valid_attrs %{name: "some name", platform: "some platform", secret_key: "some secret_key"}
    @update_attrs %{name: "some updated name", platform: "some updated platform", secret_key: "some updated secret_key"}
    @invalid_attrs %{name: nil, platform: nil, secret_key: nil}

    def bot_fixture(attrs \\ %{}) do
      {:ok, bot} =
        attrs
        |> Enum.into(@valid_attrs)
        |> BotAccounts.create_bot()

      bot
    end

    test "list_bots/0 returns all bots" do
      bot = bot_fixture()
      assert BotAccounts.list_bots() == [bot]
    end

    test "get_bot!/1 returns the bot with given id" do
      bot = bot_fixture()
      assert BotAccounts.get_bot!(bot.id) == bot
    end

    test "create_bot/1 with valid data creates a bot" do
      assert {:ok, %Bot{} = bot} = BotAccounts.create_bot(@valid_attrs)
      assert bot.name == "some name"
      assert bot.platform == "some platform"
      assert bot.secret_key == "some secret_key"
    end

    test "create_bot/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = BotAccounts.create_bot(@invalid_attrs)
    end

    test "update_bot/2 with valid data updates the bot" do
      bot = bot_fixture()
      assert {:ok, bot} = BotAccounts.update_bot(bot, @update_attrs)
      assert %Bot{} = bot
      assert bot.name == "some updated name"
      assert bot.platform == "some updated platform"
      assert bot.secret_key == "some updated secret_key"
    end

    test "update_bot/2 with invalid data returns error changeset" do
      bot = bot_fixture()
      assert {:error, %Ecto.Changeset{}} = BotAccounts.update_bot(bot, @invalid_attrs)
      assert bot == BotAccounts.get_bot!(bot.id)
    end

    test "delete_bot/1 deletes the bot" do
      bot = bot_fixture()
      assert {:ok, %Bot{}} = BotAccounts.delete_bot(bot)
      assert_raise Ecto.NoResultsError, fn -> BotAccounts.get_bot!(bot.id) end
    end

    test "change_bot/1 returns a bot changeset" do
      bot = bot_fixture()
      assert %Ecto.Changeset{} = BotAccounts.change_bot(bot)
    end
  end
end
