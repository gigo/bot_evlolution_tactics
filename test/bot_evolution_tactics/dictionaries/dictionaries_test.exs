defmodule BotEvolutionTactics.DictionariesTest do
  use BotEvolutionTactics.DataCase

  alias BotEvolutionTactics.Dictionaries
  alias BotEvolutionTactics.Factories.DictionaryFactory

  describe "dictionaries" do
    alias BotEvolutionTactics.Dictionaries.Dictionary

    @valid_attrs %{name: "hello_dictionary"}
    @update_attrs %{name: "world_dictionary"}
    @invalid_attrs %{}

    def dictionary_fixture(attrs \\ %{}) do
      DictionaryFactory.insert :dictionary, Enum.into(attrs, @valid_attrs)
    end

    test "list_dictionaries/0 returns all dictionaries" do
      dictionary = dictionary_fixture()
      assert Dictionaries.list_dictionaries() == [dictionary]
    end

    test "get_dictionary!/1 returns the dictionary with given id" do
      dictionary = dictionary_fixture()
      assert Dictionaries.get_dictionary!(dictionary.id) == dictionary
    end

    test "create_dictionary/1 with valid data creates a dictionary" do
      assert {:ok, %Dictionary{} = dictionary} = Dictionaries.create_dictionary(@valid_attrs)
    end

    test "create_dictionary/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Dictionaries.create_dictionary(@invalid_attrs)
    end

    test "update_dictionary/2 with valid data updates the dictionary" do
      dictionary = dictionary_fixture()
      assert {:ok, dictionary} = Dictionaries.update_dictionary(dictionary, @update_attrs)
      assert %Dictionary{} = dictionary
    end

    test "delete_dictionary/1 deletes the dictionary" do
      dictionary = dictionary_fixture()
      assert {:ok, %Dictionary{}} = Dictionaries.delete_dictionary(dictionary)
      assert_raise Ecto.NoResultsError, fn -> Dictionaries.get_dictionary!(dictionary.id) end
    end

    test "change_dictionary/1 returns a dictionary changeset" do
      dictionary = dictionary_fixture()
      assert %Ecto.Changeset{} = Dictionaries.change_dictionary(dictionary)
    end
  end
end
