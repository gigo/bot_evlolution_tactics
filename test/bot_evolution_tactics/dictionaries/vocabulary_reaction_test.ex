defmodule BotEvolutionTactics.VocabularyReactionTest do
  use BotEvolutionTactics.DataCase

  alias BotEvolutionTactics.Factories.VocabularyFactory

  alias BotEvolutionTactics.Repo
  alias BotEvolutionTactics.Dictionaries.Vocabulary
  alias BotEvolutionTactics.Dictionaries.Reaction

  import Ecto.Query

  describe "vocabulary / reaction association" do

    setup do
      vocabulary = VocabularyFactory.insert(:vocabulary)
      {:ok, original_vocabulary: vocabulary}
    end

    setup %{original_vocabulary: vocabulary} do
      reaction = Repo.insert! Ecto.build_assoc(vocabulary, :reactions, type: "word", response: "hello")
      {:ok, original_reaction: reaction}
    end

    test "the connetion between vocabulary/reaction exists",
    %{original_vocabulary: original_vocabulary, original_reaction: original_reaction} do
      vocabulary = Repo.one(from v in Vocabulary, where: v.id == ^original_vocabulary.id, preload: [:reactions])

      reaction = vocabulary.reactions |> Enum.at(0)
      assert reaction == original_reaction
    end

    test "the reaction can reference back to vocabulary",
    %{original_vocabulary: original_vocabulary, original_reaction: original_reaction} do
      reaction = Repo.one(from r in Reaction, where: r.id == ^original_reaction.id, preload: [:vocabulary])

      vocabulary = reaction.vocabulary
      assert vocabulary == original_vocabulary
    end

  end

end
