defmodule BotEvolutionTactics.DictionaryVocabularyTest do
  use BotEvolutionTactics.DataCase

  alias BotEvolutionTactics.Factories.DictionaryFactory

  alias BotEvolutionTactics.Repo
  alias BotEvolutionTactics.Dictionaries.Dictionary
  alias BotEvolutionTactics.Dictionaries.Vocabulary

  import Ecto.Query

  describe "dictionary / vocabulary association" do

    # construct dictionary
    setup do
      dictionary = DictionaryFactory.insert(:dictionary)
      {:ok, original_dictionary: dictionary}
    end

    # associate dictionary with a vocabulary
    setup %{original_dictionary: dictionary} do
      vocabulary = Repo.insert! Ecto.build_assoc(dictionary, :vocabularies, word: "drink", emotion_impact: 3)

      {:ok, original_vocabulary: vocabulary}
    end

    test "the connetion between dictionary/vocabulary exists",
    %{original_dictionary: original_dictionary, original_vocabulary: original_vocabulary} do
      dictionary = Repo.one(from d in Dictionary, where: d.id == ^original_dictionary.id, preload: [:vocabularies])

      vocabulary = dictionary.vocabularies |> Enum.at(0)
      assert vocabulary == original_vocabulary
    end

    test "the vocabulary can reference back to dictionary",
    %{original_dictionary: original_dictionary, original_vocabulary: original_vocabulary} do
      vocabulary = Repo.one(from v in Vocabulary, where: v.id == ^original_vocabulary.id, preload: [:dictionary])

      dictionary = vocabulary.dictionary
      assert dictionary == original_dictionary
    end

  end

end
