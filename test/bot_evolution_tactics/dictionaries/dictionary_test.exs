defmodule BotEvolutionTactics.DictionaryTest do
  use BotEvolutionTactics.DataCase

  alias BotEvolutionTactics.Dictionaries.Dictionary
  alias BotEvolutionTactics.Repo

  @valid_attrs %{name: "foo"}

  test "changeset with valid attributes" do
    changeset = Dictionary.changeset(%Dictionary{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset without name" do
    changeset = Dictionary.changeset(%Dictionary{}, Map.delete(@valid_attrs, :name))
    refute changeset.valid?
  end
end
