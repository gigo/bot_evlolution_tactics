defmodule BotEvolutionTactics.VocabularyTest do
  use BotEvolutionTactics.DataCase

  alias BotEvolutionTactics.Dictionaries.Vocabulary
  alias BotEvolutionTactics.Repo

  @valid_attrs %{word: "foo", emotion_impact: 1}

  test "changeset with valid attributes" do
    changeset = Vocabulary.changeset(%Vocabulary{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset without word" do
    changeset = Vocabulary.changeset(%Vocabulary{}, Map.delete(@valid_attrs, :word))
    refute changeset.valid?
  end
end
