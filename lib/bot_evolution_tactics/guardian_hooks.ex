defmodule BotEvolutionTactics.GuardianHooks do
  use Guardian.Hooks
  alias BotEvolutionTactics.Repo

  def after_sign_in(conn, location) do
    staff = Guardian.Plug.current_resource(conn, location)

    # update staff login at
    staff = Ecto.Changeset.change staff, last_login_at: DateTime.utc_now

    case Repo.update staff do
      {:ok, _}      -> "update success" # do nothing
      {:error, err} -> Rollbax.report(:throw, err, System.stacktrace()) # report error
    end

    conn
  end
end
