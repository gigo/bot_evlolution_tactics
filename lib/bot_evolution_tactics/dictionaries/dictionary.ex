defmodule BotEvolutionTactics.Dictionaries.Dictionary do
  use Ecto.Schema
  import Ecto.Changeset
  alias BotEvolutionTactics.Dictionaries.Dictionary


  schema "dictionaries" do
    field :name, :string
    has_many :vocabularies, BotEvolutionTactics.Dictionaries.Vocabulary
    has_many :bots_dictionaries, BotEvolutionTactics.Dictionaries.BotDictionary
    has_many :bots, through: [:bots_dictionaries, :bot]

    timestamps()
  end

  @doc false
  def changeset(%Dictionary{} = dictionary, attrs) do
    dictionary
    |> cast(attrs, [:name])
    |> cast_assoc(:vocabularies)
    |> validate_required([:name])
  end
end
