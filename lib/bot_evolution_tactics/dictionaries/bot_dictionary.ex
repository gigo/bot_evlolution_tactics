defmodule BotEvolutionTactics.Dictionaries.BotDictionary do
  use Ecto.Schema
  import Ecto.Changeset
  alias BotEvolutionTactics.Dictionaries.BotDictionary


  schema "bots_dictionaries" do
    field :apply_order,     :integer

    belongs_to :bot,        BotEvolutionTactics.BotAccounts.Bot
    belongs_to :dictionary, BotEvolutionTactics.Dictionaries.Dictionary

    timestamps()
  end

  @doc false
  def changeset(%BotDictionary{} = bot_dictionary, attrs) do
    bot_dictionary
    |> cast(attrs, [:bot, :dictionary, :apply_order])
    |> cast_assoc(:dictionary, required: true)
  end
end
