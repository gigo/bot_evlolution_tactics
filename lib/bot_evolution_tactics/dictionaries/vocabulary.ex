defmodule BotEvolutionTactics.Dictionaries.Vocabulary do
  use Ecto.Schema
  import Ecto.Changeset
  alias BotEvolutionTactics.Dictionaries.Vocabulary


  schema "vocabularies" do
    field :word,            :string
    field :emotion_impact,  :integer

    has_many   :reactions,  BotEvolutionTactics.Dictionaries.Reaction
    belongs_to :dictionary, BotEvolutionTactics.Dictionaries.Dictionary

    timestamps()
  end

  @doc false
  def changeset(%Vocabulary{} = vocabulary, attrs) do
    vocabulary
    |> cast(attrs, [:word, :emotion_impact])
    |> validate_required([:word])
    |> cast_assoc(:reactions)
    |> cast_assoc(:dictionary)
  end
end
