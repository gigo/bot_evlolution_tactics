defmodule BotEvolutionTactics.Dictionaries do
  @moduledoc """
  The Dictionaries context.
  """

  import Ecto.Query, warn: false
  alias BotEvolutionTactics.Repo

  alias BotEvolutionTactics.Dictionaries.Dictionary

  @doc """
  Returns the list of dictionaries.

  ## Examples

      iex> list_dictionaries()
      [%Dictionary{}, ...]

  """
  def list_dictionaries do
    Repo.all(Dictionary)
  end

  @doc """
  Gets a single dictionary.

  Raises `Ecto.NoResultsError` if the Dictionary does not exist.

  ## Examples

      iex> get_dictionary!(123)
      %Dictionary{}

      iex> get_dictionary!(456)
      ** (Ecto.NoResultsError)

  """
  def get_dictionary!(id), do: Repo.get!(Dictionary, id)

  @doc """
  Creates a dictionary.

  ## Examples

      iex> create_dictionary(%{field: value})
      {:ok, %Dictionary{}}

      iex> create_dictionary(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_dictionary(attrs \\ %{}) do
    %Dictionary{}
    |> Dictionary.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a dictionary.

  ## Examples

      iex> update_dictionary(dictionary, %{field: new_value})
      {:ok, %Dictionary{}}

      iex> update_dictionary(dictionary, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_dictionary(%Dictionary{} = dictionary, attrs) do
    dictionary
    |> Dictionary.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Dictionary.

  ## Examples

      iex> delete_dictionary(dictionary)
      {:ok, %Dictionary{}}

      iex> delete_dictionary(dictionary)
      {:error, %Ecto.Changeset{}}

  """
  def delete_dictionary(%Dictionary{} = dictionary) do
    Repo.delete(dictionary)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking dictionary changes.

  ## Examples

      iex> change_dictionary(dictionary)
      %Ecto.Changeset{source: %Dictionary{}}

  """
  def change_dictionary(%Dictionary{} = dictionary) do
    Dictionary.changeset(dictionary, %{})
  end
end
