defmodule BotEvolutionTactics.Dictionaries.Reaction do
  use Ecto.Schema
  import Ecto.Changeset
  alias BotEvolutionTactics.Dictionaries.Reaction

  @types [:word, :weather, :daily_news]
  def get_types, do: @types

  schema "reactions" do
    field :type,            :string
    field :response_word,   :string

    belongs_to :vocabulary, BotEvolutionTactics.Dictionaries.Vocabulary

    timestamps()
  end

  @doc false
  def changeset(%Reaction{} = reaction, attrs) do
    reaction
    |> cast(attrs, [:type, :response_word])
    |> cast_assoc(:vocabulary)
    |> validate_required([:type])
  end
end
