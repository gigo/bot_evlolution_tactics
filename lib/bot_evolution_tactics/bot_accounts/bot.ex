defmodule BotEvolutionTactics.BotAccounts.Bot do
  use Ecto.Schema
  import Ecto.Changeset
  alias BotEvolutionTactics.BotAccounts.Bot


  schema "bots" do
    field :name,              :string
    field :platform,          :string
    field :secret_key,        :string
    field :access_token,      :string
    field :account_id,        :string

    has_many :bots_dictionaries, BotEvolutionTactics.Dictionaries.BotDictionary
    has_many :dictionaries, through: [:bots_dictionaries, :dictionary]

    timestamps()
  end

  @doc false
  def changeset(%Bot{} = bot, attrs) do
    bot
    |> cast(attrs, [:name, :platform, :secret_key, :account_id, :access_token])
    |> cast_assoc(:bots_dictionaries)
    |> validate_required([:name, :platform, :secret_key])
  end
end
