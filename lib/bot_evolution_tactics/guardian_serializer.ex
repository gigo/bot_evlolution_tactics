defmodule BotEvolutionTactics.GuardianSerializer do
  @behaviour Guardian.Serializer

  alias BotEvolutionTactics.Repo
  alias BotEvolutionTactics.Staffs.Staff

  def for_token(staff = %Staff{}), do: {:ok, "Staff:#{staff.id}" }
  def for_token(_), do: {:error, "Unknown resource type"}

  def from_token("Staff:" <> id), do: {:ok, Repo.get(Staff, id) }
  def from_token(_), do: {:error, "Unknown resource type"}
end
