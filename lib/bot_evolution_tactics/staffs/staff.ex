defmodule BotEvolutionTactics.Staffs.Staff do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias BotEvolutionTactics.Staffs.Staff
  alias BotEvolutionTactics.Repo


  schema "staffs" do
    field :name,           :string
    field :password,       :string
    field :password_hash,  :string
    field :username,       :string
    field :last_login_at,  :utc_datetime

    timestamps()
  end

  @doc false
  def changeset(%Staff{} = staff, attrs) do
    staff
    |> cast(attrs, [:name, :password, :last_login_at, :username])
    |> validate_required([:name, :password, :username])
    |> unique_constraint(:username)
    |> put_password_hash
  end

  defp put_password_hash(%Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset) do
    change(changeset, Comeonin.Argon2.add_hash(password))
  end

  defp put_password_hash(changeset), do: changeset

  def find_and_confirm_password(%{"username" => username, "password" => password}) do
    Staff
    |> where(username: ^username)
    |> Repo.one
    |> Comeonin.Argon2.check_pass(password)
  end

  def find_and_confirm_password(_), do: {:error, "incorrect input data"}
end
