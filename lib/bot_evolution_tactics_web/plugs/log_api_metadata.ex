defmodule BotEvolutionTacticsWeb.Plugs.LogApiMetadata do
  use Plug.Builder

  plug :logging

  def logging(conn, _params) do
    current_time = DateTime.utc_now |> DateTime.to_iso8601

    Logger.metadata %{
      timestamp: current_time
    }

    conn
  end
end
