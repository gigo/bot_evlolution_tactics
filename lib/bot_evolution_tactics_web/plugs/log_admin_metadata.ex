defmodule BotEvolutionTacticsWeb.Plugs.LogAdminMetadata do
  use Plug.Builder

  plug :logging

  def logging(conn, _params) do
    staff = Guardian.Plug.current_resource(conn)
    current_time = DateTime.utc_now |> DateTime.to_iso8601

    Logger.metadata %{
      access_by: %{type: "staff", id: staff.id},
      timestamp: current_time
    }

    conn
  end
end
