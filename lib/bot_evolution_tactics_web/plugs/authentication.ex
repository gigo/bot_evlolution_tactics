defmodule BotEvolutionTacticsWeb.Plugs.Authentication do
  use Plug.Builder
  import Phoenix.Controller, only: [redirect: 2, put_flash: 3]

  plug Guardian.Plug.VerifySession
  plug Guardian.Plug.LoadResource
  plug Guardian.Plug.EnsureAuthenticated, handler: __MODULE__

  def unauthenticated(conn, _params) do
    alias BotEvolutionTacticsWeb.Router.Helpers, as: Routes

    conn
    |> put_status(302)
    |> put_flash(:error, "Authentication required")
    |> redirect(to: Routes.session_path(conn, :new))
    |> halt()
  end

end
