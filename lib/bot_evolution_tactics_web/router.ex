defmodule BotEvolutionTacticsWeb.Router do
  use BotEvolutionTacticsWeb, :router
  use Plug.ErrorHandler

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :admin_session do
    plug BotEvolutionTacticsWeb.Plugs.Authentication
    plug BotEvolutionTacticsWeb.Plugs.LogAdminMetadata
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug BotEvolutionTacticsWeb.Plugs.LogApiMetadata
  end

  scope "/", BotEvolutionTacticsWeb do
    pipe_through :browser # Use the default browser stack

    resources "/sessions", SessionController, only: [:new, :create, :delete]
  end

  scope "/v1", BotEvolutionTacticsWeb, as: :api_v1 do
    pipe_through :api

    get "/ping", Api.V1.PingController, :ping

    scope "/line" do
      post "/message/:bot_id", Api.V1.LineController, :message
    end
  end

  scope "/admin", BotEvolutionTacticsWeb.Admin, as: :admin do
    pipe_through [:browser, :admin_session]

    resources "/", RootController, only: [:index]
    resources "/staffs", StaffController
    resources "/bots", BotController
    resources "/dictionaries", DictionaryController
  end

  # Catch exception and report to Rollbar Service
  defp handle_errors(conn, %{kind: kind, reason: reason, stack: stacktrace}) do
    Rollbax.report(kind, reason, stacktrace)
  end

end
