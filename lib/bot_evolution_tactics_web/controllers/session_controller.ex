defmodule BotEvolutionTacticsWeb.SessionController do
  use BotEvolutionTacticsWeb, :controller
  alias BotEvolutionTactics.Staffs.Staff

  def new(conn, _params) do
    render conn, "new.html"
  end

  def create(conn, params) do
    case Staff.find_and_confirm_password(params) do
      {:ok, staff} ->
        conn
        |> Guardian.Plug.sign_in(staff)
        |> redirect(to: "/admin")
      {:error, error_message} ->
        conn
        |> put_flash(:error, error_message)
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    render conn, "new.html"
  end

end
