defmodule BotEvolutionTacticsWeb.Api.V1.LineController do
  use BotEvolutionTacticsWeb, :controller
  alias BotEvolutionTactics.BotAccounts

  def message(conn, %{"events" => events}) do
    bot = conn.params["bot_id"]
          |> BotAccounts.get_bot!

    Enum.each(events, fn e -> handle_event bot, e end)

    json conn, %{say: "hi"}
  end

  # handle text event
  defp handle_event(bot, %{"message" => %{"type" => "text"}} = event) do
    reply_token = event["replyToken"]
    timestamp   = event["timestamp"]

    _source     = event["source"]
    message     = event["message"]

    # POC, just throw back same message, simple echo
    reply_async bot, reply_token, message["text"]
  end

  # well, for other event types, let it go
  defp handle_event(bot, _), do: IO.puts "Not yet implement"

  defp reply_async(bot, token, message) do
    data = %{replyToken: token,
             messages: [%{type: "text", text: message}]}
           |> Poison.encode!

    headers = ["Authorization": "Bearer #{bot.access_token}", "Content-Type": "Application/json"]

    #TODO: future work, async version
    #HTTPoison.post "https://api.line.me/v2/bot/message/reply", data, headers, stream_to: self
    case HTTPoison.post("https://api.line.me/v2/bot/message/reply", data, headers) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        IO.puts body
      {:ok, %HTTPoison.Response{body: body}} ->
        IO.puts "Fail: #{body}"
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.puts "Http fail: #{reason}"
    end
  end
end
