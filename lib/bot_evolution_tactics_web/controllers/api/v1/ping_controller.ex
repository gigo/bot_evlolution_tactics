defmodule BotEvolutionTacticsWeb.Api.V1.PingController do
  use BotEvolutionTacticsWeb, :controller

  def ping(conn, _params), do: json(conn, %{result: "OK"})
end
