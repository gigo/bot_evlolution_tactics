defmodule BotEvolutionTacticsWeb.Admin.BotController do
  use BotEvolutionTacticsWeb, :controller

  alias BotEvolutionTactics.BotAccounts
  alias BotEvolutionTactics.BotAccounts.Bot

  def index(conn, _params) do
    bots = BotAccounts.list_bots()
    render(conn, "index.html", bots: bots)
  end

  def new(conn, _params) do
    changeset = BotAccounts.change_bot(%Bot{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"bot" => bot_params}) do
    case BotAccounts.create_bot(bot_params) do
      {:ok, bot} ->
        conn
        |> put_flash(:info, "Bot created successfully.")
        |> redirect(to: admin_bot_path(conn, :show, bot))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    bot = BotAccounts.get_bot!(id)
    render(conn, "show.html", bot: bot)
  end

  def edit(conn, %{"id" => id}) do
    bot = BotAccounts.get_bot!(id)
    changeset = BotAccounts.change_bot(bot)
    render(conn, "edit.html", bot: bot, changeset: changeset)
  end

  def update(conn, %{"id" => id, "bot" => bot_params}) do
    bot = BotAccounts.get_bot!(id)

    case BotAccounts.update_bot(bot, bot_params) do
      {:ok, bot} ->
        conn
        |> put_flash(:info, "Bot updated successfully.")
        |> redirect(to: admin_bot_path(conn, :show, bot))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", bot: bot, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    bot = BotAccounts.get_bot!(id)
    {:ok, _bot} = BotAccounts.delete_bot(bot)

    conn
    |> put_flash(:info, "Bot deleted successfully.")
    |> redirect(to: admin_bot_path(conn, :index))
  end
end
