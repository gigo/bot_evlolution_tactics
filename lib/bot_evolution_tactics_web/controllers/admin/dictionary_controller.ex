defmodule BotEvolutionTacticsWeb.Admin.DictionaryController do
  use BotEvolutionTacticsWeb, :controller

  import BotEvolutionTactics.Dictionaries

  def index(conn, _params) do
    dictionarys = list_dictionaries()
    render(conn, "index.html", dictionarys: dictionarys)
  end

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, %{"dictionary" => dictionary_params}) do
    render(conn, "new.html")
  end

  def show(conn, %{"id" => id}) do
    render(conn, "show.html")
  end

  def edit(conn, %{"id" => id}) do
    render(conn, "edit.html")
  end

  def update(conn, %{"id" => id, "dictionary" => dictionary_params}) do
    render(conn, "edit.html")
  end

  def delete(conn, %{"id" => id}) do
    redirect(conn, to: admin_dictionary_path(conn, :index))
  end
end
