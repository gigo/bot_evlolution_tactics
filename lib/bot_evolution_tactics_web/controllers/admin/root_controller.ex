defmodule BotEvolutionTacticsWeb.Admin.RootController do
  use BotEvolutionTacticsWeb, :controller

  def index(conn, _params) do
    current_staff = Guardian.Plug.current_resource(conn)
    render(conn, "index.html", current_staff: current_staff)
  end
end
