defmodule BotEvolutionTacticsWeb.LayoutView do
  use BotEvolutionTacticsWeb, :view

  def render_login_or_logout(conn) do
    case Guardian.Plug.current_resource(conn) do
      nil ->
        link "Login", to: session_path(conn, :new)
      current_staff ->
        link "Logout [#{current_staff.name}]", to: session_path(conn, :delete, current_staff.id), method: :delete
    end
  end
end
